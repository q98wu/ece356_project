from core import *

if __name__ == '__main__':
    init()
    migrate()
    # # For example, we can insert a row.
    insert_row("172.19.1.46", 52422, "10.200.7.7", 3128, 6, 131,
               "HTTP_PROXY", "26/04/201711:11:17", 22, 6, 55, 2007.52)
    cleanup()
