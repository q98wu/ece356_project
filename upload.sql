-- Temporary Table
CREATE TABLE IF NOT EXISTS csv_input (
  `Flow.ID` TEXT,
  `Source.IP` TEXT,
  `Source.Port` INTEGER,
  `Destination.IP` TEXT,
  `Destination.Port` INTEGER,
  `Protocol` INTEGER,
  `Timestamp` TEXT,
  `Flow.Duration` INTEGER,
  `Total.Fwd.Packets` INTEGER,
  `Total.Backward.Packets` INTEGER,
  `Total.Length.of.Fwd.Packets` INTEGER,
  `Total.Length.of.Bwd.Packets` INTEGER,
  `Fwd.Packet.Length.Max` INTEGER,
  `Fwd.Packet.Length.Min` INTEGER,
  `Fwd.Packet.Length.Mean` FLOAT,
  `Fwd.Packet.Length.Std` FLOAT,
  `Bwd.Packet.Length.Max` INTEGER,
  `Bwd.Packet.Length.Min` INTEGER,
  `Bwd.Packet.Length.Mean` FLOAT,
  `Bwd.Packet.Length.Std` FLOAT,
  `Flow.Bytes.s` FLOAT,
  `Flow.Packets.s` FLOAT,
  `Flow.IAT.Mean` FLOAT,
  `Flow.IAT.Std` FLOAT,
  `Flow.IAT.Max` INTEGER,
  `Flow.IAT.Min` INTEGER,
  `Fwd.IAT.Total` INTEGER,
  `Fwd.IAT.Mean` FLOAT,
  `Fwd.IAT.Std` FLOAT,
  `Fwd.IAT.Max` INTEGER,
  `Fwd.IAT.Min` INTEGER,
  `Bwd.IAT.Total` INTEGER,
  `Bwd.IAT.Mean` FLOAT,
  `Bwd.IAT.Std` FLOAT,
  `Bwd.IAT.Max` INTEGER,
  `Bwd.IAT.Min` INTEGER,
  `Fwd.PSH.Flags` INTEGER,
  `Bwd.PSH.Flags` INTEGER,
  `Fwd.URG.Flags` INTEGER,
  `Bwd.URG.Flags` INTEGER,
  `Fwd.Header.Length` INTEGER,
  `Bwd.Header.Length` INTEGER,
  `Fwd.Packets.s` FLOAT,
  `Bwd.Packets.s` FLOAT,
  `Min.Packet.Length` INTEGER,
  `Max.Packet.Length` INTEGER,
  `Packet.Length.Mean` FLOAT,
  `Packet.Length.Std` FLOAT,
  `Packet.Length.Variance` FLOAT,
  `FIN.Flag.Count` INTEGER,
  `SYN.Flag.Count` INTEGER,
  `RST.Flag.Count` INTEGER,
  `PSH.Flag.Count` INTEGER,
  `ACK.Flag.Count` INTEGER,
  `URG.Flag.Count` INTEGER,
  `CWE.Flag.Count` INTEGER,
  `ECE.Flag.Count` INTEGER,
  `Down.Up.Ratio` INTEGER,
  `Average.Packet.Size` FLOAT,
  `Avg.Fwd.Segment.Size` FLOAT,
  `Avg.Bwd.Segment.Size` FLOAT,
  `Fwd.Header.Length.1` INTEGER,
  `Fwd.Avg.Bytes.Bulk` FLOAT,
  `Fwd.Avg.Packets.Bulk` FLOAT,
  `Fwd.Avg.Bulk.Rate` FLOAT,
  `Bwd.Avg.Bytes.Bulk` FLOAT,
  `Bwd.Avg.Packets.Bulk` FLOAT,
  `Bwd.Avg.Bulk.Rate` FLOAT,
  `Subflow.Fwd.Packets` INTEGER,
  `Subflow.Fwd.Bytes` INTEGER,
  `Subflow.Bwd.Packets` INTEGER,
  `Subflow.Bwd.Bytes` INTEGER,
  `Init_Win_bytes_forward` INTEGER,
  `Init_Win_bytes_backward` INTEGER,
  `act_data_pkt_fwd` INTEGER,
  `min_seg_size_forward` INTEGER,
  `Active.Mean` FLOAT,
  `Active.Std` FLOAT,
  `Active.Max` INTEGER,
  `Active.Min` INTEGER,
  `Idle.Mean` FLOAT,
  `Idle.Std` FLOAT,
  `Idle.Max` INTEGER,
  `Idle.Min` INTEGER,
  `Label` TEXT,
  `L7Protocol` INTEGER,
  `ProtocolName` TEXT
);

-- Load Data
LOAD DATA INFILE '/var/lib/mysql-files/21-Network-Traffic/Dataset-Unicauca-Version2-87Atts.csv' INTO TABLE csv_input FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

-- Application
INSERT INTO
  Application (`IP`, `Port`)
SELECT
  `IP`,
  `Port`
FROM
  (
    SELECT
      `Source.IP` AS `IP`,
      `Source.Port` AS `Port`
    FROM
      csv_input
    UNION
    SELECT
      `Destination.IP` AS `IP`,
      `Destination.Port` AS `Port`
    FROM
      csv_input
  ) AS `IPs`
GROUP BY
  `IP`,
  `Port`;

-- ApplicationProtocol
INSERT INTO
  ApplicationProtocol (`protocolNumber`, `name`)
SELECT
  `L7Protocol` AS `protocolNumber`,
  `ProtocolName` AS `name`
FROM
  csv_input
GROUP BY
  `L7Protocol`,
  `ProtocolName`;

-- TransportProtocol
INSERT INTO
  TransportProtocol (`protocolNumber`, `name`)
VALUES
  (6, 'TCP');

INSERT INTO
  TransportProtocol (`protocolNumber`, `name`)
VALUES
  (17, 'UDP');

-- Flow
INSERT
  IGNORE INTO Flow (`sourceAppID`, `destinationAppID`)
SELECT
  (
    SELECT
      `appID`
    FROM
      Application
    WHERE
      `IP` = `Source.IP`
      AND `Port` = `Source.Port`
  ) AS `sourceAppID`,
  (
    SELECT
      `appID`
    FROM
      Application
    WHERE
      `IP` = `Destination.IP`
      AND `Port` = `Destination.Port`
  ) AS `destinationAppID`
FROM
  csv_input;

-- PacketGroup
INSERT INTO
  PacketGroup (
    `flowID`,
    `capturedTimestamp`,
    `fromSource`,
    `transportProtocolID`,
    `applicationProtocolID`
  )
SELECT
  `Flow.flowID` AS `flowID`,
  STR_TO_DATE(`csv_input`.`Timestamp`, '%d/%m/%Y %H:%i:%s') AS `capturedTimestamp`,
  1 AS `fromSource`,
  (
    SELECT
      `transportProtocolID`
    FROM
      TransportProtocol
    WHERE
      `protocolNumber` = `csv_input`.`Protocol`
  ) AS `transportProtocolID`,
  (
    SELECT
      `applicationProtocolID`
    FROM
      ApplicationProtocol
    WHERE
      `protocolNumber` = `csv_input`.`L7Protocol`
  ) AS `applicationProtocolID`
FROM
  `Flow`
  INNER JOIN `Application` AS `SourceApplication` ON `Flow`.`sourceAppID` = `SourceApplication`.`id`
  INNER JOIN `Application` AS `DestinationApplication` ON `Flow`.`destinationAppID` = `DestinationApplication`.`id`
  INNER JOIN `csv_input` ON (
    `csv_input`.`Source.IP` = `SourceApplication`.`IP`
    AND `csv_input`.`Source.Port` = `SourceApplication`.`Port`
    AND `csv_input`.`Destination.IP` = `DestinationApplication`.`IP`
    AND `csv_input`.`Destination.Port` = `DestinationApplication`.`Port`
  );

INSERT INTO
  PacketGroup (
    `flowID`,
    `capturedTimestamp`,
    `fromSource`,
    `transportProtocolID`,
    `applicationProtocolID`
  )
SELECT
  `Flow.flowID` AS `flowID`,
  STR_TO_DATE(`csv_input`.`Timestamp`, '%d/%m/%Y %H:%i:%s') AS `capturedTimestamp`,
  0 AS `fromSource`,
  (
    SELECT
      `transportProtocolID`
    FROM
      TransportProtocol
    WHERE
      `protocolNumber` = `csv_input`.`Protocol`
  ) AS `transportProtocolID`,
  (
    SELECT
      `applicationProtocolID`
    FROM
      ApplicationProtocol
    WHERE
      `protocolNumber` = `csv_input`.`L7Protocol`
  ) AS `applicationProtocolID`
FROM
  `Flow`
  INNER JOIN `Application` AS `SourceApplication` ON `Flow`.`sourceAppID` = `SourceApplication`.`id`
  INNER JOIN `Application` AS `DestinationApplication` ON `Flow`.`destinationAppID` = `DestinationApplication`.`id`
  INNER JOIN `csv_input` ON (
    `csv_input`.`Source.IP` = `DestinationApplication`.`IP`
    AND `csv_input`.`Source.Port` = `DestinationApplication`.`Port`
    AND `csv_input`.`Destination.IP` = `SourceApplication`.`IP`
    AND `csv_input`.`Destination.Port` = `SourceApplication`.`Port`
  );

-- PacketGroupStats
INSERT INTO
  PacketGroupStats (
    `flowID`,
    `capturedTimestamp`,
    `meanIAT`,
    `stdIAT`,
    `minIAT`,
    `maxIAT`,
    `meanActive`,
    `stdActive`,
    `minActive`,
    `maxActive`,
    `meanIdle`,
    `stdIdle`,
    `minIdle`,
    `maxIdle`,
    `totalFIN`,
    `totalSYN`,
    `totalRST`,
    `totalPSH`,
    `totalACK`,
    `totalURG`,
    `totalCWE`,
    `totalECE`
  )
SELECT
  (
    SELECT
      `flowID`
    FROM
      `Flow`
    WHERE
      `sourceAppID` = (
        SELECT
          `appID`
        FROM
          `Application`
        WHERE
          `IP` = SUBSTRING_INDEX(
            SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 1),
            '-',
            -1
          )
          AND `Port` = CAST(
            SUBSTRING_INDEX(
              SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 3),
              '-',
              -1
            ) AS INT
          )
      ),
      `destinationAppID` = (
        SELECT
          `appID`
        FROM
          `Application`
        WHERE
          `IP` = SUBSTRING_INDEX(
            SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
            '-',
            1
          )
          AND `Port` = CAST(
            SUBSTRING_INDEX(
              SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
              '-',
              3
            ) AS INT
          )
      )
  ) AS `flowID`,
  STR_TO_DATE(`csv_input`.`Timestamp`, '%d/%m/%Y %H:%i:%s') AS `capturedTimestamp`,
  CAST(`csv_input`.`Flow.IAT.Mean` AS FLOAT) AS `meanIAT`,
  CAST(`csv_input`.`Flow.IAT.Std` AS FLOAT) AS `stdIAT`,
  CAST(`csv_input`.`Flow.IAT.Min` AS DECIMAL) AS `minIAT`,
  CAST(`csv_input`.`Flow.IAT.Max` AS DECIMAL) AS `maxIAT`,
  CAST(`csv_input`.`Active.Mean` AS FLOAT) AS `meanActive`,
  CAST(`csv_input`.`Active.Std` AS FLOAT) AS `stdActive`,
  CAST(`csv_input`.`Active.Min` AS DECIMAL) AS `minActive`,
  CAST(`csv_input`.`Active.Max` AS DECIMAL) AS `maxActive`,
  CAST(`csv_input`.`Idle.Mean` AS FLOAT) AS `meanIdle`,
  CAST(`csv_input`.`Idle.Std` AS FLOAT) AS `stdIdle`,
  CAST(`csv_input`.`Idle.Min` AS DECIMAL) AS `minIdle`,
  CAST(`csv_input`.`Idle.Max` AS DECIMAL) AS `maxIdle`,
  CAST(`csv_input`.`FIN.Flag.Count` AS INT) AS `totalFIN`,
  CAST(`csv_input`.`SYN.Flag.Count` AS INT) AS `totalSYN`,
  CAST(`csv_input`.`RST.Flag.Count` AS INT) AS `totalRST`,
  CAST(`csv_input`.`PSH.Flag.Count` AS INT) AS `totalPSH`,
  CAST(`csv_input`.`ACK.Flag.Count` AS INT) AS `totalACK`,
  CAST(`csv_input`.`URG.Flag.Count` AS INT) AS `totalURG`,
  CAST(`csv_input`.`CWE.Flag.Count` AS INT) AS `totalCWE`,
  CAST(`csv_input`.`ECE.Flag.Count` AS INT) AS `totalECE`
FROM
  csv_input;

-- TransportStats
INSERT INTO
  TransportStats(
    `flowID`,
    `capturedTimestamp`,
    `totalSize`,
    `avgLength`,
    `stdLength`,
    `meanIAT`,
    `stdIAT`,
    `minIAT`,
    `maxIAT`,
    `downUpRatio`,
    `totalPSH`,
    `totalURG`
  )
SELECT
  (
    SELECT
      `flowID`
    FROM
      `Flow`
    WHERE
      `sourceAppID` = (
        SELECT
          `appID`
        FROM
          `Application`
        WHERE
          `IP` = SUBSTRING_INDEX(
            SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 1),
            '-',
            -1
          )
          AND `Port` = CAST(
            SUBSTRING_INDEX(
              SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 3),
              '-',
              -1
            ) AS INT
          )
      ),
      `destinationAppID` = (
        SELECT
          `appID`
        FROM
          `Application`
        WHERE
          `IP` = SUBSTRING_INDEX(
            SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
            '-',
            1
          )
          AND `Port` = CAST(
            SUBSTRING_INDEX(
              SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
              '-',
              3
            ) AS INT
          )
      )
  ) AS `flowID`,
  STR_TO_DATE(`csv_input`.`Timestamp`, '%d/%m/%Y %H:%i:%s') AS `capturedTimestamp`,
  `Total.Length.of.Fwd.Packets` + `Total.Length.of.Bwd.Packets` AS `totalSize`,
  CAST(`csv_input`.`Packet.Length.Mean` AS DECIMAL) AS `avgLength`,
  CAST(`csv_input`.`Packet.Length.Std` AS DECIMAL) AS `stdLength`,
  CAST(`csv_input`.`Flow.IAT.Mean` AS FLOAT) AS `meanIAT`,
  CAST(`csv_input`.`Flow.IAT.Std` AS FLOAT) AS `stdIAT`,
  CAST(`csv_input`.`Flow.IAT.Min` AS DECIMAL) AS `minIAT`,
  CAST(`csv_input`.`Flow.IAT.Max` AS DECIMAL) AS `maxIAT`,
  CAST(`csv_input`.`Down.Up.Ratio` AS FLOAT) AS `downUpRatio`,
  CAST(`csv_input`.`Fwd.PSH.Flags` AS DECIMAL) + CAST(`csv_input`.`Bwd.PSH.Flags` AS DECIMAL) AS `totalPSH`,
  CAST(`csv_input`.`Fwd.URG.Flags` AS DECIMAL) + CAST(`csv_input`.`Bwd.URG.Flags` AS DECIMAL) AS `totalURG`
)
FROM
  csv_input;

-- PacketsObserveTransportStats
INSERT INTO
  PacketsObserveTransportStats(
    `flowID`,
    `capturedTimestamp`,
    `fwdTransportStatsID`,
    `bwdTransportStatsID`
  )
SELECT
  (
    SELECT
      `flowID`
    FROM
      `Flow`
    WHERE
      `sourceAppID` = (
        SELECT
          `appID`
        FROM
          `Application`
        WHERE
          `IP` = SUBSTRING_INDEX(
            SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 1),
            '-',
            -1
          )
          AND `Port` = CAST(
            SUBSTRING_INDEX(
              SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 3),
              '-',
              -1
            ) AS INT
          )
      ),
      `destinationAppID` = (
        SELECT
          `appID`
        FROM
          `Application`
        WHERE
          `IP` = SUBSTRING_INDEX(
            SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
            '-',
            1
          )
          AND `Port` = CAST(
            SUBSTRING_INDEX(
              SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
              '-',
              3
            ) AS INT
          )
      )
  ) AS `flowID`,
  STR_TO_DATE(`csv_input`.`Timestamp`, '%d/%m/%Y %H:%i:%s') AS `capturedTimestamp`,
  (
    SELECT
      `transportStatsID`
    FROM
      `TransportStats`
    WHERE
      `flowID` = (
        SELECT
          `flowID`
        FROM
          `Flow`
        WHERE
          `sourceAppID` = (
            SELECT
              `appID`
            FROM
              `Application`
            WHERE
              `IP` = SUBSTRING_INDEX(
                SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 1),
                '-',
                -1
              )
              AND `Port` = CAST(
                SUBSTRING_INDEX(
                  SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 3),
                  '-',
                  -1
                ) AS INT
              )
          ),
          `destinationAppID` = (
            SELECT
              `appID`
            FROM
              `Application`
            WHERE
              `IP` = SUBSTRING_INDEX(
                SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
                '-',
                1
              )
              AND `Port` = CAST(
                SUBSTRING_INDEX(
                  SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
                  '-',
                  3
                ) AS INT
              )
          )
      )
  ) AS `fwdTransportStatsID`,
  (
    SELECT
      `transportStatsID`
    FROM
      `TransportStats`
    WHERE
      `flowID` = (
        SELECT
          `flowID`
        FROM
          `Flow`
        WHERE
          `sourceAppID` = (
            SELECT
              `appID`
            FROM
              `Application`
            WHERE
              `IP` = SUBSTRING_INDEX(
                SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 1),
                '-',
                -1
              )
              AND `Port` = CAST(
                SUBSTRING_INDEX(
                  SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', 3),
                  '-',
                  -1
                ) AS INT
              )
          ),
          `destinationAppID` = (
            SELECT
              `appID`
            FROM
              `Application`
            WHERE
              `IP` = SUBSTRING_INDEX(
                SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
                '-',
                1
              )
              AND `Port` = CAST(
                SUBSTRING_INDEX(
                  SUBSTRING_INDEX(`csv_input`.`Flow.ID`, '-', -1),
                  '-',
                  3
                ) AS INT
              )
          )
      )
  ) AS `bwdTransportStatsID`
FROM
  csv_input;

-- Delete Temporary Table
DROP TABLE csv_input;