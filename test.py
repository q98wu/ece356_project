from core import *
import unittest

class TestDB(unittest.TestCase):
    def test_get_applications(self):
        res = get_applications()
        self.assertEqual(len(res), 4)
        self.assertEqual(res[0][1], "192.68.100.20")
        self.assertEqual(res[0][2], 8080)
        self.assertEqual(res[1][1], "172.20.20.20")
        self.assertEqual(res[1][2], 8000)
        self.assertEqual(res[2][1], "192.80.80.20")
        self.assertEqual(res[2][2], 10)
        self.assertEqual(res[3][1], "162.100.20.80")
        self.assertEqual(res[3][2], 5)

    def test_get_application_with_id(self):
        res = get_applications(1)
        self.assertEqual(res[0][1], "192.68.100.20")
        self.assertEqual(res[0][2], 8080)

    def test_get_application_with_ip_port(self):
        res = get_application_source_port("192.68.100.20", 8080)
        self.assertEqual(res[0], 1)

    def test_insert_application(self):
        ip, port = "192.10.100.20", 10
        res = insert_application(ip, port)
        self.assertEqual(res[1], ip)
        self.assertEqual(res[2], port)

    def test_get_application_protocol(self):
        src, dst = get_applications_with_app_protocol("HTTP")
        self.assertEqual(src[0][0], "192.68.100.20")
        self.assertEqual(src[0][1], 8080)
        self.assertEqual(dst[0][0], "172.20.20.20")
        self.assertEqual(dst[0][1], 8000)

    def test_get_app_protocols(self):
        protocols = get_app_protocols()
        self.assertEqual(len(protocols), 2)
        self.assertEqual(protocols[0][2], "HTTP")
        self.assertEqual(protocols[1][2], "GOOGLE")
        res = get_app_protocols(1)
        self.assertEqual(res[0][0], 1)

    def test_insert_app_protocol(self):
        num, name = 91, "SSL"
        res = insert_app_protocol(num, name)
        self.assertEqual(res[1], num)
        self.assertEqual(res[2], name)

    def test_modify_app_protocol(self):
        appid, new_num, new_name = 3, 131, "HTTP_PROXY"
        modify_app_protocol(appid, new_num, new_name)
        res = get_app_protocols(appid)
        self.assertEqual(res[0][1], new_num)
        self.assertEqual(res[0][2], new_name)

    def test_get_trans_protocols(self):
        res = get_trans_protocols()
        self.assertEqual(len(res), 2)
        self.assertEqual(res[0][1], 6)
        self.assertEqual(res[0][2], "TCP")
        self.assertEqual(res[1][1], 17)
        self.assertEqual(res[1][2], "UDP")
        res = get_trans_protocols(1)
        self.assertEqual(res[0][1], 6)
        self.assertEqual(res[0][2], "TCP")
        res = get_trans_protocol_number_name(17, "UDP")
        self.assertEqual(res[0], 2)

    def test_get_flows(self):
        sourceID, destID = 1, 2
        res = get_flows(1)
        self.assertEqual(res[0][1], sourceID)
        self.assertEqual(res[0][2], destID)

    def test_get_timestamps_with_flowid(self):
        res = get_timestamps_with_flowid(1)
        self.assertEqual(res[0][0].strftime('%Y-%m-%d %H:%M:%S'), "2017-04-26 11:11:17")

    def test_get_flow_between(self):
        res = get_flow_between(1, 2)
        self.assertEqual(res[0], 1)

    def test_insert_flow(self):
        res = insert_flow(1, 3)
        self.assertEqual(res[0], 3)
        self.assertEqual(res[1], 1)
        self.assertEqual(res[2], 3)

    def test_get_packet_group_for_flow(self):
        flowid, timestamp = 1, "26/04/2017 11:11:17"
        res = get_packet_group_for_flow(flowid)
        self.assertEqual(res[0][1].strftime('%Y-%m-%d %H:%M:%S'), "2017-04-26 11:11:17")
        res = get_packet_group_for_flow(flowid, timestamp)
        self.assertEqual(res[0], flowid)
        
    def test_get_packet_group_stats(self):
        res = get_packet_group_stats(1)
        self.assertEqual(res[1].strftime('%Y-%m-%d %H:%M:%S'), "2017-04-26 11:11:17")
        self.assertEqual(res[3], 72.33)
        res = get_packet_group_stats_in_range(1, "26/04/2017 11:11:17", "26/04/2019 11:11:17")
        self.assertEqual(res[0][1].strftime('%Y-%m-%d %H:%M:%S'), "2017-04-26 11:11:17")
        self.assertEqual(res[0][3], 72.33)
        
    def test_insert_packet_group_stats(self):
        iat = Stats(9090,1,1,1)
        active = Stats(2,2,2,2)
        idle = Stats(3,3,3,3)
        stats = PacketGroupStats(iat, active, idle, 1, 1, 1, 1, 1, 1, 1, 1)
        res = insert_packet_group_stats(1, "26/04/2017 11:11:17", stats)
        self.assertEqual(res[3], 9090)
        
    def test_modify_packet_group_stats(self):
        modify_packet_group_stats(1, "26/04/2017 11:11:17", ["meanIAT"], [111])
        res = get_packet_group_stats(1)
        self.assertEqual(res[3], 111)
        
        
    def test_get_transport_stats(self):
        res = get_transport_stats(1)
        self.assertEqual(res[1].strftime('%Y-%m-%d %H:%M:%S'), "2017-04-26 11:11:17")
        self.assertEqual(res[3], 3000)
        fwd, bwd = get_transport_stats_in_range(1, "26/04/2017 11:11:17", "26/04/2019 11:11:17")
        self.assertEqual(fwd[0][3], 3000)
        self.assertEqual(bwd[0][3], 4000)
        
    def test_insert_transport_stats(self):
        stats = TransportStats(1,1,1,Stats(0,0,0,0),1,1,1)
        res = insert_transport_stats(1, "26/04/2017 11:11:17", stats)
        self.assertEqual(res[1].strftime('%Y-%m-%d %H:%M:%S'), "2017-04-26 11:11:17")
        
    def test_modify_transport_stats(self):
        modify_transport_stats(1, "26/04/2017 11:11:17", ["totalSize"], [100])
        res = get_transport_stats(5)
        self.assertEqual(res[3], 100)
        
if __name__ == '__main__':
    init()
    migrate()
    setup_test()

    # run tests
    unittest.main()

    cleanup()


