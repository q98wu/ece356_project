from core import *

class Client:
    def connect_db(self):
        init()
        migrate()
        setup_test()

    def disconnect_db(self):
        cleanup()    

    def create_application(self):
        params = input("Enter the IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        if len(params) == 2:
            if get_application_source_port(params[0], params[1]) is not None:
                print("An application already exists with", params[0], params[1])
            else:  
              return insert_application(params[0], params[1])[0]
        else:
            self.print_input_error()
      
    def create_application_protocol(self):
        params = input("Enter the protocol number and name (Ex: 7 HTTP): ").split(" ")
        if len(params) == 2:
            if get_app_protocol_name_number(params[1], params[0]) is not None:
                print("A application protocol already exists with name", params[0], params[1])
            else:
              return insert_app_protocol(params[0], params[1])[0]
        else:
            self.print_input_error()
      
    def create_flow(self):
        params = input("Enter the source IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        src = get_application_source_port(params[0], params[1])
        if src is None:
            src = insert_application(params[0], params[1])
        params = input("Enter the destination IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        dest = get_application_source_port(params[0], params[1])
        if dest is None:
            dest = insert_application(params[0], params[1])
        success = insert_flow(src[0], dest[0])
        if not success:
            print("A flow with your entered information already exists.")
        else:
            return success[0]
      
    def create_packet_group(self):
        params = input("Enter the source IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        src = get_application_source_port(params[0], params[1])
        if src is None:
            src = insert_application(params[0], params[1])
        params = input("Enter the destination IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        dest = get_application_source_port(params[0], params[1])
        if dest is None:
            dest = insert_application(params[0], params[1])
        flow = get_flow_between(src[0], dest[0])
        if flow is None:
            flow = insert_flow(src[0], dest[0])
        params = input("Enter the protocol number and name (Ex: 7 HTTP): ").split(" ")
        app_protocol = get_app_protocol_name_number(params[1], params[0])
        if app_protocol is None:
            app_protocol = insert_app_protocol(params[0], params[1])
        transportProtocol = input("Enter the transport protocol (TCP/UDP): ")
        timestamp = input("Enter the timestamp (Ex: 26/04/2017 11:11:17): ")
        fromSource = input("Did this packet group originate from the source application (yes/no): ")
        if flow[0] and app_protocol[0] and timestamp and fromSource and (transportProtocol == "TCP" or transportProtocol == "UDP"):
            if get_packet_group_for_flow(flow[0], timestamp) != None:
                print("A packet group already exists with matching flow and timestamp")
            else:
                transportID = get_trans_protocol_number_name(6 if transportProtocol == "TCP" else 17, transportProtocol)[0]
                insert_packet_group(flow[0], timestamp, transportID, app_protocol[0], True if fromSource == "yes" else False)
                self.create_packet_group_stats(flow[0], timestamp)
                statid = self.create_transport_stats(flow[0], timestamp)
                insert_packet_observes_stats(flow[0], timestamp, statid[0], statid[1])
        else:
            self.print_input_error()

    def create_packet_group_stats(self, flowID, timestamp):
        IAT = input("Enter the mean, standard deviation, max, and min values for IAT (Ex. 5 3 2 1): ").split(" ")
        active = input("Enter the mean, standard deviation, max, and min values for active time (Ex. 5 3 2 1): ").split(" ")
        idle = input("Enter the mean, standard deviation, max, and min values for idle time (Ex. 5 3 2 1): ").split(" ")
        if len(IAT) == 4 and len(active) == 4 and len(idle) == 4:
            IAT_stats = Stats(*IAT)
            active_stats = Stats(*active)
            idle_stats = Stats(*idle)
            flags = input("Enter how many FIN, SYN, RST, PSH, ACK, URG, CWE, and ECE flags were used (Ex: 1 2 3 ...): ").split(" ")
            packet_group_stats = PacketGroupStats(IAT_stats, active_stats, idle_stats, *flags)
            success = insert_packet_group_stats(flowID, timestamp, packet_group_stats)
            if not success:
                print("Stats already exist for this packet group. Please update them instead.")
            else:
                return success[2]
        else:
            self.print_input_error()

    def create_transport_stats(self, flowID, timestamp):
        directions = ["forward", "backword"] 
        result = []
        for direction in directions:
            print("for the ", direction, " direction:")
            pkt = input("Enter the total, average and std length (Ex: 1 2 3): ").split(" ")
            IAT = input("Enter the mean, standard deviation, max, and min values for IAT (Ex: 5 3 2 1): ").split(" ")
            downupratio = input("Enter the down up ratio (Ex: 0.5): ")
            flags = input ("Enter how many PSH and URG flags were used (Ex: 1 2): ").split(" ")
            if len(IAT) == 4 and len(pkt) == 3 and len(flags) == 2:
                transport_stats = TransportStats(pkt[0], pkt[1], pkt[2], Stats(*IAT), downupratio, flags[0], flags[1])
                success = insert_transport_stats(flowID, timestamp, transport_stats)
                if not success:
                    print(direction, " transport stats already exist for this packetgroup. Please update them instead.")
                else:
                    result.append(success[2])
            else:
                self.print_input_error()
                return
        return result

    def modify_application(self):
        params = input("Enter the IP address and port of the application you want to modify (Ex: 192.168.163.19 8000): ").split(" ")
        application = get_application_source_port(*params)
        if application is None:
            print("This application does not exist.")
            return
        values = input("Enter the modified IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        if len(values) == 2:
            modify_application(application[0], *values)
        else:
            self.print_input_error()
      
    def modify_application_protocol(self):
        params = input("Enter the protocol number and name of the protocol you want to modify (Ex: 7 HTTP): ").split(" ")
        protocol = get_app_protocol_name_number(params[1], params[0])
        if protocol is None:
            print("This protocol does not exist.")
            return
        values = input("Enter the modified protocol number and name (Ex: 7 HTTP): ").split(" ")
        if len(params) == 2:
            modify_app_protocol(protocol[0], *values)
        else:
            self.print_input_error()

    def modify_packet_group_stats(self):
        params = input("Enter the source IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        src = get_application_source_port(params[0], params[1])
        params = input("Enter the destination IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        dest = get_application_source_port(params[0], params[1])
        if src is None or dest is None:
            print("Application not found.")
            return
        flow = get_flow_between(src[0], dest[0])
        if flow is None:
            print("Flow does not exist.")
            return
        timestamp = input("Enter the timestamp (Ex: 26/04/2017 11:11:17): ")
        attrNames = ["meanIAT", "stdIAT", "minIAT", "maxIAT", "meanActive", "stdActive", "minActive", "maxActive", "meanIdle", "stdIdle", "minIdle", "maxIdle", "totalFIN", "totalSYN", "totalRST", "totalPSH", "totalACK", "totalURG", "totalCWE", "totalECE"]
        print("List of attributes:")
        for i, name in enumerate(attrNames):
            print(str(i+1) + ". " + name)
        attr = input("Enter the names of the attributes you want to modify (Ex: maxIAT minIAT): ").split(" ")
        values = input("Enter the values of the attributes you want to modify (Ex: 2 2): ").split(" ")
        if len(attr) == len(values) and len(attr) > 0:
            modify_packet_group_stats(flow[0], timestamp, attr, values)
        else:
            self.print_input_error()
        
    def modify_transport_stats(self):      
        params = input("Enter the source IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        src = get_application_source_port(params[0], params[1])
        params = input("Enter the destination IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        dest = get_application_source_port(params[0], params[1])
        if src is None or dest is None:
            print("Application not found.")
            return
        flow = get_flow_between(src[0], dest[0])
        if flow is None:
            print("Flow does not exist.")
            return
        timestamp = input("Enter the timestamp (Ex: 26/04/2017 11:11:17): ")
        attrNames = ["totalSize", "avgLength", "stdLength", "meanIAT", "stdIAT", "minIAT", "maxIAT", "downUpRatio", "totalPSH", "totalURG"]
        print("List of attributes:")
        for i, name in enumerate(attrNames):
            print(str(i+1) + ". " + name)
        attr = input("Enter the names of the attributes you want to modify (Ex: avg_len min_iat): ").split(" ")
        values = input("Enter the values of the attributes you want to modify (Ex: 2 2): ").split(" ")
        if len(attr) == len(values) and len(attr) > 0:
            modify_transport_stats(flow[0], timestamp, attr, values)
        else:
            self.print_input_error()

    def query_ports(self):
        ip = input("Enter an IP address (Ex: 192.168.163.19): ")
        ports = get_ports_with_ip(ip)
        if len(ports) == 0:
            print("No results.")
            return
        for port in ports:
            print(port[0])

    def query_app_protocol_number(self):
        name = input("Enter protocol name (Ex: HTTPS): ")
        num = get_app_protocol_name_number(name)
        if num is None:
            print("No results.")
            return
        print(num[1])

    def query_timestamps(self):
        params = input("Enter the source IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        src = get_application_source_port(params[0], params[1])
        params = input("Enter the destination IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        dest = get_application_source_port(params[0], params[1])
        if src is None or dest is None:
            print("No results.")
            return
        flowID = get_flow_between(src[0], dest[0])
        if flowID is None:
            print("No results.")
            return
        timestamps = get_timestamps_with_flowid(flowID[0])
        if len(timestamps) == 0:
            print("No results.")
            return
        for timestamp in timestamps:
            print(timestamp[0].strftime("%d/%m/%Y %H:%M:%S"))

    def query_packet_group_stats(self):
        params = input("Enter the source IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        src = get_application_source_port(params[0], params[1])
        params = input("Enter the destination IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        dest = get_application_source_port(params[0], params[1])
        if src is None or dest is None:
            print("No results.")
            return
        flowID = get_flow_between(src[0], dest[0])
        if flowID is None:
            print("No results.")
            return
        params = input("Enter the start and end times (Ex: 26/04/2017 11:11:17 27/04/2017 11:11:17): ").split(" ")
        stats = get_packet_group_stats_in_range(flowID[0], params[0] + " " +  params[1], params[2] + " " + params[3])
        attrNames = ["meanIAT", "stdIAT", "minIAT", "maxIAT", "meanActive", "stdActive", "minActive", "maxActive", "meanIdle", "stdIdle", "minIdle", "maxIdle", "totalFIN", "totalSYN", "totalRST", "totalPSH", "totalACK", "totalURG", "totalCWE", "totalECE"]
        for stat in stats:
            row = ""
            print("Timestamp: ", stat[1])
            for i in range(3, len(stat)):
                row += attrNames[i-3] + ": " + str(stat[i])
                if i != len(stat) - 1:
                    row += ", "
            print(row)

    def query_transport_stats(self):
        params = input("Enter the source IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        src = get_application_source_port(params[0], params[1])
        params = input("Enter the destination IP address and port (Ex: 192.168.163.19 8000): ").split(" ")
        dest = get_application_source_port(params[0], params[1])
        if src is None or dest is None:
            print("No results.")
            return
        flowID = get_flow_between(src[0], dest[0])
        if flowID is None:
            print("No results.")
            return
        params = input("Enter the start and end times (Ex: 26/04/2017 11:11:17 27/04/2017 11:11:17): ").split(" ")
        fwds, bwds = get_transport_stats_in_range(flowID[0], params[0] + " " + params[1], params[2] + " " + params[3])
        attrNames = ["total_size", "avg_len", "std_len", "mean_iat", "std_iat", "min_iat", "max_iat", "down_up_ratio", "psh", "urg"]
        for i in range(len(fwds)):
            row = ""
            print("Timestamp: ", fwds[i][1])
            for j in range(3, len(fwds[i])):
                row += attrNames[i-3] + ": " + str(fwds[i][j])
                if j != len(fwds[i]) - 1:
                    row += ", "
            print("Forward stats: ")
            print(row)
            row = ""
            for j in range(3, len(bwds[i])):
                row += attrNames[i-3] + ": " + str(bwds[i][j])
                if i != len(bwds[i]) - 1:
                    row += ", "
            print("Backward stats: ")
            print(row)

    def query_applications(self):
        protocol_name = input("Enter the application protocol name (Ex: HTTPS): ")
        src_list, dest_list = get_flows_with_app_protocol(protocol_name)
        for i in range(len(src_list)):
            print("source IP: " + src_list[i][0] +  ", source port: " +  str(src_list[i][1]) +  ", destination IP: " + dest_list[i][0] + ", destination port: " + str(dest_list[i][1]))

    def start(self):
        print("----- Welcome to the Network Statistics DB Console -----")
        role = input("Enter role (user or superuser): ")
        while role != "user" and role != "superuser":
            role = input("Enter role (user or superuser): ")
        self.print_help(role)
        self.connect_db()
        ans = ""
        while True:
            ans = input(">> ")
            if ans == "exit":
                break
            elif ans == "create application" and role == "superuser":
                self.create_application()
            elif ans == "create application protocol" and role == "superuser":
                self.create_application_protocol()
            elif ans == "create flow" and role == "superuser":
                self.create_flow()
            elif ans == "create packet group" and role == "superuser":
                self.create_packet_group()
            elif ans == "modify application" and role == "superuser":
                self.modify_application()
            elif ans == "modify application protocol" and role == "superuser":
                self.modify_application_protocol()
            elif ans == "modify packet group stats" and role == "superuser":
                self.modify_packet_group_stats()
            elif ans == "modify transport stats" and role == "superuser":
                self.modify_transport_stats()
            elif ans == "query ports":
                self.query_ports()
            elif ans == "query application protocol number":
                self.query_app_protocol_number()
            elif ans == "query timestamps":
                self.query_timestamps()
            elif ans == "query packet group stats":
                self.query_packet_group_stats()
            elif ans == "query transport stats":
                self.query_transport_stats()
            elif ans == "query flows by protocol":
                self.query_applications()
            elif ans == "help":
                print_help(role)
            else:
                print("Invalid command! Type 'help' to look at the available commands.")

        self.disconnect_db()
        
    def print_help(self, role):
        print("Available commands:")
        if role == "superuser":
            print("create application")
            print("create application protocol")
            print("create flow")
            print("create packet group")
            print("modify application")
            print("modify application protocol")
            print("modify packet group stats")
            print("modify transport stats")
        print("query ports") # get ports used by ip
        print("query application protocol number") # get protocol number given protocol name
        print("query timestamps") # get timestamps of all packet groups in a flow
        print("query packet group stats") # get the packet group stats in a flow during some time range
        print("query transport stats") # get the transport stats in a flow during some time range
        print("query flows by protocol") # get source and destination IPs and ports given an application protocol name
        print("exit")
        
    def print_input_error(self):
        print("There was an error with your input format.")
      

if __name__ == '__main__':
    client = Client()
    #try:
    client.start()
   # except Exception as e:
    #    client.disconnect_db()
     #   print(e)
