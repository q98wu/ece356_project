CREATE TABLE IF NOT EXISTS Application (
	appID int AUTO_INCREMENT,
	IP varchar(15),
	port int,
	primary key (appID),
	unique index (IP, port)
);

CREATE TABLE IF NOT EXISTS ApplicationProtocol(
	applicationProtocolID int AUTO_INCREMENT,
	protocolNumber decimal(3, 0),
	name varchar(30),
	primary key (applicationProtocolID),
	unique index (protocolNumber, name)
);

CREATE TABlE IF NOT EXISTS TransportProtocol(
	transportProtocolID int AUTO_INCREMENT,
	protocolNumber decimal(3, 0),
	name varchar(30),
	primary key (transportProtocolID),
	unique index (protocolNumber, name)
);

CREATE TABLE IF NOT EXISTS Flow (
	flowID int AUTO_INCREMENT,
	sourceAppID int,
	destinationAppID int,
	primary key (flowID),
	foreign key (sourceAppID) references Application(appID),
	foreign key (destinationAppID) references Application(appID),
	unique index (sourceAppID, destinationAppID)
);

CREATE TABLE IF NOT EXISTS PacketGroup(
	flowID int,
	capturedTimestamp datetime,
	fromSource boolean,
	transportProtocolID int,
	applicationProtocolID int,
	primary key (flowID, capturedTimestamp),
	foreign key (flowID) references Flow(flowID),
	foreign key (transportProtocolID) references TransportProtocol(transportProtocolID),
	foreign key (applicationProtocolID) references ApplicationProtocol(applicationProtocolID)
);


CREATE TABLE IF NOT EXISTS PacketGroupStats(
	flowID int,
	capturedTimestamp datetime,
	pgStatsID int AUTO_INCREMENT,
	meanIAT float,
	stdIAT float,
	minIAT decimal,
	maxIAT decimal,
	meanActive float,
	stdActive float,
	minActive decimal,
	maxActive decimal,
	meanIdle float,
	stdIdle float,
	minIdle decimal,
	maxIdle decimal,
	totalFIN int,
	totalSYN int,
	totalRST int,
	totalPSH int,
	totalACK int,
	totalURG int,
	totalCWE int,
	totalECE int,
	primary key (pgStatsID, flowID, capturedTimestamp),
	foreign key (flowID, capturedTimestamp) references PacketGroup(flowID, capturedTimestamp)
);

CREATE TABLE IF NOT EXISTS TransportStats(
	flowID int,
	capturedTimestamp datetime,
	transportStatsID int AUTO_INCREMENT,
	totalSize int,
	avgLength decimal,
	stdLength decimal,
	meanIAT float,
	stdIAT float,
	minIAT decimal,
	maxIAT decimal,
	downUpRatio float,
	totalPSH decimal,
	totalURG decimal,
	primary key (transportStatsID, flowID, capturedTimestamp),
	foreign key (flowID, capturedTimestamp) references PacketGroup(flowID, capturedTimestamp)
);

CREATE TABLE IF NOT EXISTS PacketsObserveTransportStats(
	flowID int,
	capturedTimestamp datetime,
	fwdTransportStatsID int,
	bwdTransportStatsID int,
	primary key(flowID, capturedTimestamp),
	foreign key (flowID, capturedTimestamp) references PacketGroup(flowID, capturedTimestamp),
	foreign key (fwdTransportStatsID) references TransportStats(transportStatsID),
	foreign key (bwdTransportStatsID) references TransportStats(transportStatsID)
);