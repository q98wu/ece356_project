-- create new test DB
-- DROP DATABASE IF EXISTS testDB;
-- CREATE DATABASE testDB;
-- USE testDB;
-- create tables and constraints
-- START schema.sql;

-- insert test data
INSERT INTO Application (`IP`, `port`) VALUES ("192.68.100.20", 8080);
INSERT INTO Application (`IP`, `port`) VALUES ("172.20.20.20", 8000);
INSERT INTO Application (`IP`, `port`) VALUES ("192.80.80.20", 10);
INSERT INTO Application (`IP`, `port`) VALUES ("162.100.20.80", 5);

INSERT INTO ApplicationProtocol(`protocolNumber`, `name`) VALUES (7, "HTTP");
INSERT INTO ApplicationProtocol(`protocolNumber`, `name`) VALUES (126, "GOOGLE");

INSERT INTO TransportProtocol(`protocolNumber`, `name`) VALUES (6, "TCP");
INSERT INTO TransportProtocol(`protocolNumber`, `name`) VALUES (17, "UDP");

INSERT INTO Flow(`sourceAppID`, `destinationAppID`) VALUES (1,2);
INSERT INTO Flow(`sourceAppID`, `destinationAppID`) VALUES (3,4);

INSERT INTO PacketGroup(
            `flowID`,
            `capturedTimestamp`,
            `fromSource`,
            `transportProtocolID`,
            `applicationProtocolID`
        ) VALUES (1, STR_TO_DATE("26/04/2017 11:11:17", "%d/%m/%Y %H:%i:%s"), 1, 1, 1);
INSERT INTO PacketGroup(
            `flowID`,
            `capturedTimestamp`,
            `fromSource`,
            `transportProtocolID`,
            `applicationProtocolID`
        ) VALUES (2, STR_TO_DATE("17/05/2018 09:23:14", "%d/%m/%Y %H:%i:%s"), 0, 2, 2);

INSERT INTO PacketGroupStats (
            `flowID`,
            `capturedTimestamp`,
            `meanIAT`,
            `stdIAT`,
            `minIAT`,
            `maxIAT`,
            `meanActive`,
            `stdActive`,
            `minActive`,
            `maxActive`,
            `meanIdle`,
            `stdIdle`, 
            `minIdle`,
            `maxIdle`,
            `totalFIN`,
            `totalSYN`,
            `totalRST`,
            `totalPSH`,
            `totalACK`,
            `totalURG`,
            `totalCWE`,
            `totalECE`
        ) VALUES (1, STR_TO_DATE("26/04/2017 11:11:17", "%d/%m/%Y %H:%i:%s"), 72.33, 62.66, 110, 20, 5, 0, 6, 7, 3, 3, 6, 0, 1, 2, 0, 4, 0, 2, 1, 2);
INSERT INTO PacketGroupStats (
            `flowID`,
            `capturedTimestamp`,
            `meanIAT`,
            `stdIAT`,
            `minIAT`,
            `maxIAT`,
            `meanActive`,
            `stdActive`,
            `minActive`,
            `maxActive`,
            `meanIdle`,
            `stdIdle`, 
            `minIdle`,
            `maxIdle`,
            `totalFIN`,
            `totalSYN`,
            `totalRST`,
            `totalPSH`,
            `totalACK`,
            `totalURG`,
            `totalCWE`,
            `totalECE`
        ) VALUES (2, STR_TO_DATE("17/05/2018 09:23:14", "%d/%m/%Y %H:%i:%s"), 2000, 11, 200, 12, 3, 4, 6, 10, 1, 0, 2, 3, 2, 0, 3, 0, 0, 2, 1, 1);

INSERT INTO TransportStats (
            `flowID`,
            `capturedTimestamp`,
            `totalSize`,
            `avgLength`,
            `stdLength`,
            `meanIAT`,
            `stdIAT`,
            `minIAT`,
            `maxIAT`,
            `downUpRatio`,
            `totalPSH`,
            `totalURG`
        ) VALUES (1, STR_TO_DATE("26/04/2017 11:11:17", "%d/%m/%Y %H:%i:%s"), 3000, 1000, 50, 80, 70, 20, 200, 2, 5, 10);
INSERT INTO TransportStats (
            `flowID`,
            `capturedTimestamp`,
            `totalSize`,
            `avgLength`,
            `stdLength`,
            `meanIAT`,
            `stdIAT`,
            `minIAT`,
            `maxIAT`,
            `downUpRatio`,
            `totalPSH`,
            `totalURG`
        ) VALUES (1, STR_TO_DATE("26/04/2017 11:11:17", "%d/%m/%Y %H:%i:%s"), 4000, 900, 40, 70, 60, 15, 150, 3, 8, 8);
INSERT INTO TransportStats (
            `flowID`,
            `capturedTimestamp`,
            `totalSize`,
            `avgLength`,
            `stdLength`,
            `meanIAT`,
            `stdIAT`,
            `minIAT`,
            `maxIAT`,
            `downUpRatio`,
            `totalPSH`,
            `totalURG`
        ) VALUES (2, STR_TO_DATE("17/05/2018 09:23:14", "%d/%m/%Y %H:%i:%s"), 2000, 1500, 30, 75, 20, 10, 100, 3, 6, 1);
INSERT INTO TransportStats (
            `flowID`,
            `capturedTimestamp`,
            `totalSize`,
            `avgLength`,
            `stdLength`,
            `meanIAT`,
            `stdIAT`,
            `minIAT`,
            `maxIAT`,
            `downUpRatio`,
            `totalPSH`,
            `totalURG`
        ) VALUES (2, STR_TO_DATE("17/05/2018 09:23:14", "%d/%m/%Y %H:%i:%s"), 2100, 1600, 35, 30, 25, 15, 150, 6, 0, 2);

INSERT INTO PacketsObserveTransportStats (
            `flowID`,
            `capturedTimestamp`,
            `fwdTransportStatsID`,
            `bwdTransportStatsID`
        ) VALUES (1, STR_TO_DATE("26/04/2017 11:11:17", "%d/%m/%Y %H:%i:%s"), 1, 2);
INSERT INTO PacketsObserveTransportStats (
            `flowID`,
            `capturedTimestamp`,
            `fwdTransportStatsID`,
            `bwdTransportStatsID`
        ) VALUES (2, STR_TO_DATE("17/05/2018 09:23:14", "%d/%m/%Y %H:%i:%s"), 3, 4);
