import MySQLdb
import ipaddress
import datetime

# maybe sample or client files can use these classes to store info


class Stats:
    def __init__(self, mean, std, maximum, minimum):
        self.mean = mean
        self.std = std
        self.minimum = minimum
        self.maximum = maximum


class PacketGroupStats:
    def __init__(self, iat, active, idle, fin, syn, rst, psh, ack, urg, cwe, ece):
        self.iat = iat
        self.active = active
        self.idle = idle
        self.fin = fin
        self.syn = syn
        self.rst = rst
        self.psh = psh
        self.ack = ack
        self.urg = urg
        self.cwe = cwe
        self.ece = ece


class TransportStats:
    def __init__(self, total_size, avg_len, std_len, iat, down_up_ratio, psh, urg):
        self.total_size = total_size
        self.avg_len = avg_len
        self.std_len = std_len
        self.iat = iat
        self.down_up_ratio = down_up_ratio
        self.psh = psh
        self.urg = urg


def core_print(msg):
    """
    Print a message to the terminal.
    """
    print("[core] " + msg)


def init(host="containers-us-west-25.railway.app",
         user="root", passwd="t3wKQn4IYvb0ibhlMCck", db="railway", port=5590):
    """
    Initialize the database connection.
    """
    global conn
    conn = MySQLdb.connect(host=host, user=user,
                           passwd=passwd, db=db, port=port)
    core_print("connected to mysql")


def setup_test():
    cursor = conn.cursor()
    cursor.execute("DROP DATABASE IF EXISTS testDB;")
    cursor.execute("CREATE DATABASE testDB;")
    cursor.execute("USE testDB;")
    sql = open("schema.sql").read()
    cursor.execute(sql)
    sql = open("test_setup.sql").read()
    cursor.execute(sql)


def cleanup():
    """
    Cleanup the database connection.
    """
    if conn is not None:
        conn.close()
        core_print("disconnected from mysql")


def migrate():
    """
    Run migrations on the database if required.
    """
    with open("schema.sql", "r") as f:
        sql = f.read()
        statements = sql.split(";")
        cursor = conn.cursor()
        for statement in statements:
            trimmed = statement.strip()
            if trimmed != "":
                cursor.execute(trimmed)
        conn.commit()
        cursor.close()
        core_print("migrated mysql database")


def get_applications(appid=None):
    """
    Get all applications from the database, or an application by id.
    """
    cursor = conn.cursor()
    if appid is None:
        cursor.execute("SELECT * FROM Application ORDER BY appID")
    else:
        cursor.execute(
            "SELECT * FROM Application WHERE appID = %s ORDER BY appID", (int(appid),))
    result = cursor.fetchall()
    cursor.close()
    return result


def get_application_source_port(source, port):
    """
    Get an application with a given source/port.
    """
    cursor = conn.cursor()
    cursor.execute(
        "SELECT * FROM Application WHERE IP = %s AND port = %s", (source, int(port), ))
    result = cursor.fetchone()
    cursor.close()
    return result


def get_ports_with_ip(ip):
    cursor = conn.cursor()
    cursor.execute("SELECT port FROM Application WHERE IP = %s", (ip, ))
    result = cursor.fetchall()
    cursor.close()
    return result


def get_flows_with_app_protocol(name):
    flow = "SELECT flowID, sourceAppID, destinationAppID FROM Flow INNER JOIN PacketGroup USING (flowID) INNER JOIN ApplicationProtocol USING (applicationProtocolID) WHERE name=\"" + name + "\""
    sql1 = "WITH Flows AS (" + flow + \
        ") SELECT IP, port FROM Application INNER JOIN Flows ON (sourceAppID=appID)"
    sql2 = "WITH Flows AS (" + flow + \
        ") SELECT IP, port FROM Application INNER JOIN Flows ON (destinationAppID=appID)"
    cursor = conn.cursor()
    cursor.execute(sql1)
    src = cursor.fetchall()
    cursor.execute(sql2)
    dest = cursor.fetchall()
    cursor.close()
    return (src, dest)


def insert_application(appid, port):
    """
    Inserts a new application into the database.
    """
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO Application (IP, port) VALUES (%s, %s)", (appid, int(port), ))
    conn.commit()
    cursor.close()
    return get_applications(cursor.lastrowid)[0]


def modify_application(appid, new_ip, new_port):
    cursor = conn.cursor()
    cursor.execute(
        "UPDATE Application SET IP = %s, port = %s WHERE appID = %s", (new_ip, int(new_port), appid, ))
    conn.commit()
    cursor.close()


def get_app_protocols(protocolid=None):
    """
    Get all application protocols from the database, or a protocol by id.
    """
    cursor = conn.cursor()
    if protocolid is None:
        cursor.execute(
            "SELECT * FROM ApplicationProtocol ORDER BY applicationProtocolID")
    else:
        cursor.execute(
            "SELECT * FROM ApplicationProtocol WHERE applicationProtocolID = %s ORDER BY applicationProtocolID", (protocolid,))
    result = cursor.fetchall()
    cursor.close()
    return result


def get_app_protocol_name_number(name, num=None):
    """
    Get the application protocol number and name for a given protocol.
    """
    cursor = conn.cursor()
    if num is not None:
        cursor.execute("SELECT * FROM ApplicationProtocol WHERE name = %s AND protocolNumber = %s",
                       (name, int(num),))
    else:
        cursor.execute(
            "SELECT * FROM ApplicationProtocol WHERE name = %s", (name,))
    result = cursor.fetchone()
    cursor.close()
    return result


def insert_app_protocol(num, name):
    """
    Insert a new application protocol into the database.
    """
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO ApplicationProtocol (protocolNumber, name) VALUES (%s, %s)", (int(num), name, ))
    conn.commit()
    cursor.close()
    return get_app_protocols(cursor.lastrowid)[0]


def modify_app_protocol(appid, new_num, new_name):
    cursor = conn.cursor()
    cursor.execute(
        "UPDATE ApplicationProtocol SET protocolNumber = %s, name = %s WHERE applicationProtocolID = %s", (int(new_num), new_name, appid, ))
    conn.commit()
    cursor.close()


def get_trans_protocols(protocolid=None):
    """
    Get all transport protocols from the database, or a protocol by id.
    """
    cursor = conn.cursor()
    if protocolid is None:
        cursor.execute(
            "SELECT * FROM TransportProtocol ORDER BY transportProtocolID")
    else:
        cursor.execute(
            "SELECT * FROM TransportProtocol WHERE transportProtocolID = %s ORDER BY transportProtocolID", (protocolid,))
    result = cursor.fetchall()
    cursor.close()
    return result


def get_trans_protocol_number_name(num, name):
    """
    Get the transport protocol number and name for a given protocol.
    """
    cursor = conn.cursor()
    cursor.execute(
        "SELECT * FROM TransportProtocol WHERE protocolNumber = %s AND name = %s", (int(num), name, ))
    result = cursor.fetchone()
    cursor.close()
    return result


def insert_trans_protocol(num, name):
    """
    Insert a new transport protocol into the database.
    """
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO TransportProtocol (protocolNumber, name) VALUES (%s, %s)", (int(num), name, ))
    conn.commit()
    cursor.close()
    return get_trans_protocols(cursor.lastrowid)[0]


def get_timestamps_with_flowid(flowid):
    cursor = conn.cursor()
    cursor.execute(
        "SELECT capturedTimestamp FROM PacketGroup WHERE flowID = %s", (int(flowid), ))
    result = cursor.fetchall()
    cursor.close()
    return result


def get_flows(flowid=None):
    """
    Get all flows from the database, or a flow by id.
    """
    cursor = conn.cursor()
    if flowid is None:
        cursor.execute("SELECT * FROM Flow ORDER BY flowID")
    else:
        cursor.execute(
            "SELECT * FROM Flow WHERE flowID = %s ORDER BY flowID", (int(flowid),))
    result = cursor.fetchall()
    cursor.close()
    return result


def get_flow_between(srcid, dstid):
    """
    Get a flow between two applications.
    """
    cursor = conn.cursor()
    cursor.execute(
        "SELECT * FROM Flow WHERE sourceAppID = %s AND destinationAppID = %s", (int(srcid), int(dstid), ))
    result = cursor.fetchone()
    cursor.close()
    return result


def insert_flow(srcid, dstid):
    """
    Insert a new flow between two applications.
    """
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO Flow (sourceAppID, destinationAppID) VALUES (%s, %s)", (int(srcid), int(dstid), ))
    conn.commit()
    cursor.close()
    return get_flows(cursor.lastrowid)[0]

# Either get all packet groups for a flow or get one packet group entry if you specify the timestamp as well


def get_packet_group_for_flow(flowid, timestamp=None):
    """
    Get all packet groups for a flow.
    """
    cursor = conn.cursor()
    if timestamp == None:
        cursor.execute(
            "SELECT * FROM PacketGroup WHERE flowID = %s", (int(flowid),))
        result = cursor.fetchall()
        cursor.close()
        return result
    else:
        ts = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S")
        cursor.execute("SELECT * FROM PacketGroup WHERE flowID = %s AND capturedTimestamp = %s",
                       (int(flowid), ts.strftime('%Y-%m-%d %H:%M:%S'),))
        result = cursor.fetchone()
        cursor.close()
        return result


def insert_packet_group(flowid, timestamp, transportProtoId, applicationProtoId, from_src):
    """
    Insert a new packet group.
    """
    # Example Timestamp = 26/04/201711:11:17
    ts = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S")
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO PacketGroup (flowID, capturedTimestamp, transportProtocolID, applicationProtocolID, fromSource) VALUES (%s, %s, %s, %s, %s)",
        (int(flowid), ts.strftime('%Y-%m-%d %H:%M:%S'), int(transportProtoId), int(applicationProtoId), bool(from_src), ))
    conn.commit()
    cursor.close()


def get_packet_group_stats(statsid):
    """
    Get the stats for a packet group statsID.
    """
    cursor = conn.cursor()
    cursor.execute(
        "SELECT * FROM PacketGroupStats WHERE pgStatsID = %s", (int(statsid),))
    result = cursor.fetchone()
    cursor.close()
    return result


def get_packet_group_stats_in_range(flowid, start, end):
    start_ts = datetime.datetime.strptime(start, "%d/%m/%Y %H:%M:%S")
    end_ts = datetime.datetime.strptime(end, "%d/%m/%Y %H:%M:%S")
    cursor = conn.cursor()
    cursor.execute(
        "SELECT * FROM PacketGroupStats WHERE flowID = %s AND capturedTimestamp BETWEEN %s AND %s", (int(flowid), start_ts.strftime('%Y-%m-%d %H:%M:%S'), end_ts.strftime('%Y-%m-%d %H:%M:%S'),))
    result = cursor.fetchall()
    cursor.close()
    return result


def insert_packet_group_stats(flowid, timestamp, stats):
    """
    Insert a new packet group stats entry.
    """
    iat, active, idle = stats.iat, stats.active, stats.idle
    ts = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S")
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO PacketGroupStats (flowID, capturedTimestamp, meanIAT, stdIAT, minIAT, maxIAT, meanActive, stdActive, minActive, maxActive, meanIdle, stdIdle, minIdle, maxIdle, \
        totalFIN, totalSYN, totalRST, totalPSH, totalACK, totalURG, totalCWE, totalECE) \
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
        (int(flowid), ts.strftime('%Y-%m-%d %H:%M:%S'), float(iat.mean), float(iat.std), int(iat.minimum), int(iat.maximum), float(active.mean), float(active.std), int(active.minimum), int(active.maximum),
            float(idle.mean), float(idle.std), int(idle.minimum), int(idle.maximum), stats.fin, stats.syn, stats.rst, stats.psh, stats.ack, stats.urg, stats.cwe, stats.ece, ))
    conn.commit()
    cursor.close()
    return get_packet_group_stats(cursor.lastrowid)


def modify_packet_group_stats(flowid, timestamp, attr, val):
    cursor = conn.cursor()
    ts = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S")
    query = "UPDATE PacketGroupStats SET "
    for i in range(len(attr)):
        query += attr[i] + "=" + str(val[i])
        if i is not len(attr)-1:
            query += ","
    query += " WHERE flowID=" + \
        str(flowid) + " AND capturedTimestamp=\"" + \
        ts.strftime('%Y-%m-%d %H:%M:%S') + "\""
    cursor.execute(query)
    conn.commit()
    cursor.close()


def get_transport_stats(statsid):
    cursor = conn.cursor()
    cursor.execute(
        "SELECT * FROM TransportStats WHERE transportStatsID = %s", (int(statsid),))
    result = cursor.fetchone()
    cursor.close()
    return result


def get_transport_stats_in_range(flowid, start, end):
    start_ts = datetime.datetime.strptime(start, "%d/%m/%Y %H:%M:%S")
    end_ts = datetime.datetime.strptime(end, "%d/%m/%Y %H:%M:%S")
    cursor = conn.cursor()
    cursor.execute(
        "SELECT * FROM TransportStats WHERE transportStatsID = (SELECT fwdTransportStatsID FROM PacketsObserveTransportStats WHERE flowID = %s AND capturedTimestamp BETWEEN %s AND %s )", (int(flowid), start_ts.strftime('%Y-%m-%d %H:%M:%S'), end_ts.strftime('%Y-%m-%d %H:%M:%S'),))
    fwd = cursor.fetchall()
    cursor.execute(
        "SELECT * FROM TransportStats WHERE transportStatsID = (SELECT bwdTransportStatsID FROM PacketsObserveTransportStats WHERE flowID = %s AND capturedTimestamp BETWEEN %s AND %s )", (int(flowid), start_ts.strftime('%Y-%m-%d %H:%M:%S'), end_ts.strftime('%Y-%m-%d %H:%M:%S'),))
    bwd = cursor.fetchall()
    cursor.close()
    return (fwd, bwd)


def insert_transport_stats(flowid, timestamp, stats):
    """
    Insert a new transport stats entry.
    """
    #total_size, avg_len, std_len, iat, down_up_ratio, psh, urg
    ts = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S")
    iat = stats.iat
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO TransportStats (flowID, capturedTimestamp, totalSize, avgLength, stdLength, meanIAT, stdIAT, minIAT, maxIAT, downUpRatio, totalPSH, totalURG) \
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
        (int(flowid), ts.strftime('%Y-%m-%d %H:%M:%S'), int(stats.total_size), float(stats.avg_len), float(stats.std_len), float(iat.mean),
         float(iat.std), int(iat.minimum), int(iat.maximum), float(stats.down_up_ratio), int(stats.psh), int(stats.urg), ))
    conn.commit()
    cursor.close()
    return get_transport_stats(cursor.lastrowid)


def modify_transport_stats(flowid, timestamp, attr, val):
    cursor = conn.cursor()
    ts = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S")
    query = "UPDATE TransportStats SET "
    for i in range(len(attr)):
        query += attr[i] + "=" + str(val[i])
        if i is not len(attr)-1:
            query += ","
    query += " WHERE flowID=" + \
        str(flowid) + " AND capturedTimestamp=\"" + \
        ts.strftime('%Y-%m-%d %H:%M:%S') + "\""
    cursor.execute(query)
    conn.commit()
    cursor.close()


def insert_packet_observes_stats(flowid, timestamp, fwdTransportStatsID, bwdTransportStatsID):
    ts = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S")
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO PacketsObserveTransportStats (flowID, capturedTimestamp, fwdTransportStatsID, bwdTransportStatsID) VALUES (%s, %s, %s, %s)",
        (int(flowid), ts.strftime('%Y-%m-%d %H:%M:%S'), int(fwdTransportStatsID), int(fwdTransportStatsID), ))
    conn.commit()
    cursor.close()

# packet_group_stats, fwd_trans_stats, bwd_trans_stats are classes built by the client


def insert_row(src_ip, src_port, dst_ip, dst_port, transport_proto, l7_proto_num, l7_proto_name, timestamp, packet_group_stats, fwd_trans_stats, bwd_trans_stats):
    """
    Insert a single CICFlowmeter row into the database.
    """
    # Get the source/destination application objects.
    src = get_application_source_port(src_ip, src_port)
    if src is None:
        src = insert_application(src_ip, src_port)
    dst = get_application_source_port(dst_ip, dst_port)
    if dst is None:
        dst = insert_application(dst_ip, dst_port)
    # Get the flow between the two applications.
    flow = get_flow_between(src[0], dst[0])
    if flow is None:
        flow = insert_flow(src[0], dst[0])
    # Get the protocol object for the transport layer.
    name = "UDP" if transport_proto == 17 else "TCP" if transport_proto == 6 else ""
    if name == "":
        raise Exception("Unknown transport protocol: " + str(transport_proto))
    transportp = get_trans_protocol_number_name(transport_proto, name)
    if transportp is None:
        transportp = insert_trans_protocol(transport_proto, name)
    # Get the protocol object for the application layer.
    applicationp = get_app_protocol_number_name(l7_proto_num, l7_proto_name)
    if applicationp is None:
        applicationp = insert_app_protocol(l7_proto_num, l7_proto_name)

    # Add the stats
    insert_packet_group_stats(flow[0], timestamp, packet_group_stats)
    fwd_transport_stats = insert_transport_stats(
        flowp[0], timestamp, fwd_trans_stats)
    bwd_transport_stats = insert_transport_stats(
        flowp[0], timestamp, bwd_trans_stats)
    # Relationship entity with forward, backward stats
    insert_packet_observes_stats(
        flow[0], timestamp, fwd_transport_stats[0], bwd_transport_stats[0])
    # somehow determine fromSource variable. Maybe get it from user input
    from_src = True  # change this

    # Finally, we can now insert the packet group.
    insert_packet_group(flow[0], timestamp,
                        transportp[0], applicationp[0], from_src)
